module.exports = {
  "*.js": "eslint --fix",
  "*.{scss,sass}": "stylelint --fix",
  "*.{php,module,inc,install,test,profile,theme}":
    "bin/phpcbf --standard=Drupal,DrupalPractice --extensions=php,module,inc,install,test,profile,theme",
};
