// Get gulp components and templates.
const { series, parallel, watch } = require("gulp");
const { css, lib, sass } = require("@coldfrontlabs/gulp-templates");

const { argv } = require("yargs/yargs")(process.argv.slice(2));

let mode = "";

try {
  mode = argv.mode;
} catch (error) {
  mode = "production";
}

const paths = {
  css: {
    src: "dist/css",
    dest: "dist/css",
    selector: "**/*.css",
  },
  lib: {
    src: [
      "node_modules/bootstrap/dist/js/bootstrap.min.js",
      "node_modules/popper.js/dist/umd/popper.min.js",
    ],
    dest: "dist/lib",
  },
  sass: {
    src: "scss",
    dest: "scss",
    selector: "**/*.scss",
  },
  min: "**/*.min.*",
  map: "**/*.map",
};

/**
 * Lints all Sass files.
 *
 * @return {Object} - Gulp stream.
 */
const lintStyles = () =>
  sass.lint({ source: `${paths.sass.src}/${paths.sass.selector}` });
lintStyles.description = "Lints all Sass files.";

/**
 * Lints and fixes all Sass files.
 *
 * This also generates a list of ignored files based on .stylelintignore.
 * This is to address the bug found here: https://github.com/olegskl/gulp-stylelint/issues/112
 *
 * @return {Object} - Gulp stream.
 */
const fixStyles = () => {
  let stylelintIgnore = [];

  try {
    // eslint-disable-next-line global-require
    const fs = require("fs");
    const data = fs.readFileSync("./.stylelintignore", "utf8");

    stylelintIgnore = data
      .split("\n")
      .filter(line => line !== "" && !line.startsWith("#"))
      .map(line => `!${line}`);
  } catch (error) {
    stylelintIgnore = [];
  }

  return sass.fix({
    source: [`${paths.sass.src}/${paths.sass.selector}`, ...stylelintIgnore],
  });
};
fixStyles.description = "Lints and fixes all Sass files.";

/**
 * Compiles all Sass files using dart sass and autoprefixer.
 *
 * @return {Object} - Gulp stream.
 */
const compileStyles = () =>
  sass.compile({
    source: `${paths.sass.src}/${paths.sass.selector}`,
    destination: paths.css.dest,
    sourcemap: mode === "development",
  });
compileStyles.description =
  "Compiles all Sass files using dart sass and autoprefixer.";

/**
 * Minifies all CSS files.
 *
 * @return {Object} - Gulp stream.
 */
const minifyStyles = () =>
  css.minify({
    source: [
      `${paths.css.src}/${paths.css.selector}`,
      `!${paths.min}`,
      `!${paths.map}`,
    ],
    destination: paths.css.dest,
    sourcemap: mode === "development",
    sourcemapOptions: { loadMaps: true },
  });
minifyStyles.description = "Minifies all CSS files.";

/**
 * Gathers all required libraries.
 *
 * @return {Object} - Gulp stream.
 */
const fetchLibs = () =>
  lib.fetch({
    source: paths.lib.src,
    destination: paths.lib.dest,
    sourceOptions: { base: "./node_modules/" },
  });
fetchLibs.description = "Gathers all required libraries.";

/**
 * A placeholder function to run as an alternative to other tasks.
 *
 * The purpose of this function is to allow for the build task to
 * dynamically change the jobs it runs depending on envionment and sources.
 *
 * @param  {Function} callback - Gulps callback function.
 *
 * @return {Object} - Gulp stream.
 */
const placeholder = callback => callback();

/**
 * Compiles and minifies all Sass/CSS files.
 *
 * @return {Object} - Gulp stream.
 */
const build = parallel(
  series(
    mode === "development" ? lintStyles : placeholder,
    compileStyles,
    minifyStyles
  ),
  paths.lib.src.length > 0 ? fetchLibs : placeholder
);
build.description = "Compiles and minifies all Sass/CSS files.";

/**
 * Watches all Sass files and lints, compiles, and minifies them.
 */
function watchFiles() {
  watch(
    `${paths.sass.src}/${paths.sass.selector}`,
    series(lintStyles, compileStyles, minifyStyles)
  );
}
watchFiles.description =
  "Watches all Sass files and lints, compiles, and minifies them.";

// Create default tasks
exports.build = build;
exports.watch = watchFiles;

exports.default = build;
